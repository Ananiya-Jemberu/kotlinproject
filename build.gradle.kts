import java.lang.invoke.MethodHandles.invoker

plugins {
    java
    kotlin("jvm") version "1.3.71"
}

group = "org.example"
version = "1.0-SNAPSHOT"
val invoker by configurations.creating
repositories {
    mavenCentral()
}
tasks.register("runFunction", JavaExec::class){
    main = "com.google.cloud.functions.invoker.runner.Invoker"
    classpath(configurations.invoke { invoker })
    inputs.files(configurations.runtimeClasspath, sourceSets.main.invoke { output })
    args("--target", project.findProperty("runFunction.target") ?: "hello",
               "--port", project.findProperty("runFunction.port") ?: 8080)
    doFirst {
        args("--classpath",files(configurations.runtimeClasspath, sourceSets.main.invoke { output }).asPath)
    }
}
dependencies {
    invoker("com.google.cloud.functions.invoker:java-function-invoker:1.0.0-alpha-2-rc5")
    implementation("com.google.cloud.functions:functions-framework-api:1.0.1")
    implementation(kotlin("stdlib-jdk8"))
    testImplementation("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}